# Zeddit
[ ![Codeship Status for hiEven/zeddit](https://app.codeship.com/projects/d84a6f90-2e7b-0135-7b43-7aac707f50eb/status?branch=master)](https://app.codeship.com/projects/225106)

Please check Zeddit on https://zeddit-hieven.herokuapp.com/

- [Requirements](#markdown-header-requirements)
- [Install](#markdown-header-install)
- [Dependency (Do I use boilerplate ?)](#markdown-header-dependency)
- [Run Developer Mode](#markdown-header-run-developer-mode)
- [Run Production Mode](#markdown-header-run-production-mode)
- [Run Test](#markdown-header-run-test)
- [Folder Structure](#markdown-header-folder-structure)
- [Memory Storage](#markdown-header-memory-storage)
	- [Config](#markdown-header-config-example)
	- [Flow](#markdown-header-flow)
	- [Create](#markdown-header-create)
	- [Update](#markdown-header-update)
- [Memory Lock](#markdown-header-memory-lock)
	- [Config](#markdown-header-config)
	- [Lock](#markdown-header-lock)
	- [Unlock](#markdown-header-unlock)
- [IOC layer](#markdown-header-ioc-layer)
- [Continuous Integration / Continuous Deployment](#markdown-header-ci-cd)

## Requirements
- Node.js 8

## Install
```sh
$ yarn
```

## Dependency
- client
  - React
  - React-Router
  - Redux
  - Redux-Saga
  - Redux-Form
  - Immutable
  - Webpack
- server
  - Koa
  - Koa Router
  - Joi

No boilerplate or scaffolding tools used. All code are all written from scratch.

## Run Developer mode
```sh
$ cp config/index.js config/development.js # make changes on development.js
$ npm run dev & npm run build:dev
```

## Run Production mode
```sh
$ cp config/index.js config/production.js # make changes on production.js
$ npm run build
$ npm start
```

## Run Test
```sh
$ npm t # or `npm run test:cov` with coverage
```

### Result
File                           |  % Stmts | % Branch |  % Funcs |  % Lines |Uncovered Lines |
-------------------------------|----------|----------|----------|----------|----------------|
All files                      |    97.25 |    84.81 |    97.09 |    97.78 |                |


## Folder Structure
```sh
├── Procfile
├── README.md
├── client
│   ├── public
│   │   └── build # compiled client code
│   ├── src
│   │   ├── actions    # action creators
│   │   ├── components # dummy components
│   │   ├── constants  # action types
│   │   ├── containers # smart components
│   │   ├── index.jsx  # entry point
│   │   ├── reducers   # redux reducers
│   │   ├── routes.jsx # routes of react router
│   │   ├── sagas      # redux saga
│   │   ├── selectors  # common selectors
│   │   ├── services   # api wrapper
│   │   ├── store      # redux store
│   │   └── utils      # utils
│   ├── webpack
│   │   ├── common.config.js # shared config
│   │   ├── dev.config.js    # dev config
│   │   └── prod.config.js   # prod config
│   └── webpack.config.js
├── config
│   ├── development.js
│   └── index.js
├── package.json
├── server
│   ├── container.js     # IOC layer
│   ├── context
│   │   └── render.js    # koa context method
│   ├── errors           # customized error class
│   │   ├── AppError.js
│   │   ├── ErrorCode.js
│   │   ├── Forbidden.js
│   │   ├── NotFound.js
│   │   └── index.js
│   ├── index.js
│   ├── models           # domain
│   │   └── Topic.js
│   ├── routes
│   │   ├── index.js
│   │   ├── middlewares
│   │   │   ├── apiLogger.js
│   │   │   ├── checkTopicExist.js
│   │   │   ├── csrf.js
│   │   │   └── errorRecover.js
│   │   └── v1           # API version control
│   │       ├── forms    # pre-validate client input
│   │       ├── index.js
│   │       └── topics   # topics RESTful API
│   ├── server.js
│   ├── services         # services layer
│   │   └── topics
│   │       ├── create.js
│   │       ├── findById.js
│   │       ├── format.js
│   │       ├── formatList.js
│   │       ├── increment.js
│   │       └── list.js
│   ├── utils
│   │   ├── joi.js
│   │   ├── logger.js
│   │   ├── memlock.js
│   │   ├── memorm.js
│   │   └── pug.js
│   └── views
│       └── index.pug
└── yarn.lock
```

## Memory Storage
### Config (example)
```js
{
  // table name
  prefix: 'topics',

  // Joi validation object
  schema: {},

  // fields which need sorting
  orderBy: []
}
```

### Flow

#### Initial status
```json
{
  "topics:$store": {},
  "topics:$orderBy:createdAt": [],
  "topics:$orderBy:voteCount": []
}
```

#### Insert 1 topic
`topics:$store`: 1 new record
`topics:$orderBy:createdAt`: 1 new record
`topics:$orderBy:voteCount`: 1 new record
```json
{
  "topics:$store": {
    "879353c7-c3cd-4588-a4a8-f6570c154bfc": {
      "content": "hello",
      "id": "879353c7-c3cd-4588-a4a8-f6570c154bfc",
      "voteCount": 0,
      "createdAt": "2017-06-04T18:07:29.802Z",
      "updatedAt": "2017-06-04T18:07:29.802Z"
    }
  },
  "topics:$orderBy:createdAt": [
    "879353c7-c3cd-4588-a4a8-f6570c154bfc"
  ],
  "topics:$orderBy:voteCount": [
    "879353c7-c3cd-4588-a4a8-f6570c154bfc"
  ]
}
```

#### Insert another 1 topic.
`topics:$store`: 1 new record
`topics:$orderBy:createdAt`: 1 new record
`topics:$orderBy:voteCount`: 1 new record
```json
{
  "topics:$store": {
    "879353c7-c3cd-4588-a4a8-f6570c154bfc": {
      "content": "hello",
      "id": "879353c7-c3cd-4588-a4a8-f6570c154bfc",
      "voteCount": 0,
      "createdAt": "2017-06-04T18:07:29.802Z",
      "updatedAt": "2017-06-04T18:07:29.802Z"
    },
    "11e98baa-0dda-49ec-a162-4d68e44f27a8": {
      "content": "world",
      "id": "11e98baa-0dda-49ec-a162-4d68e44f27a8",
      "voteCount": 0,
      "createdAt": "2017-06-04T18:08:43.547Z",
      "updatedAt": "2017-06-04T18:08:43.547Z"
    }
  },
  "topics:$orderBy:createdAt": [
    "11e98baa-0dda-49ec-a162-4d68e44f27a8",
    "879353c7-c3cd-4588-a4a8-f6570c154bfc"
  ],
  "topics:$orderBy:voteCount": [
    "11e98baa-0dda-49ec-a162-4d68e44f27a8",
    "879353c7-c3cd-4588-a4a8-f6570c154bfc"
  ]
}
```

Upvote `879353c7-c3cd-4588-a4a8-f6570c154bfc` topic.
It will re-order `topics:$orderBy:voteCount`
```json
{
  "topics:$store": {
    "879353c7-c3cd-4588-a4a8-f6570c154bfc": {
      "content": "hello",
      "id": "879353c7-c3cd-4588-a4a8-f6570c154bfc",
      "voteCount": 1,
      "createdAt": "2017-06-04T18:07:29.802Z",
      "updatedAt": "2017-06-04T18:09:44.422Z"
    },
    "11e98baa-0dda-49ec-a162-4d68e44f27a8": {
      "content": "world",
      "id": "11e98baa-0dda-49ec-a162-4d68e44f27a8",
      "voteCount": 0,
      "createdAt": "2017-06-04T18:08:43.547Z",
      "updatedAt": "2017-06-04T18:08:43.547Z"
    }
  },
  "topics:$orderBy:createdAt": [
    "11e98baa-0dda-49ec-a162-4d68e44f27a8",
    "879353c7-c3cd-4588-a4a8-f6570c154bfc"
  ],
  "topics:$orderBy:voteCount": [
    "879353c7-c3cd-4588-a4a8-f6570c154bfc",
    "11e98baa-0dda-49ec-a162-4d68e44f27a8"
  ]
}
```

### Create
#### Step 1
Do schema validation and set default value for any `undefined`

#### Step 2
Set `createdAt` and `updatedAt` to current time

#### Step 3
Save data into memory `${ModelName}:$store`

#### Step 4
Iterate fields in `${Model}.orderBy` and then use [binary search](https://bitbucket.org/hiEven/zeddit/src/d1a33a0d0da1aa92f626681b5052ded41cabc029/server/utils/memorm.js?at=master&fileviewer=file-view-default#memorm.js-41) to find the best position for it to insert.

### Update
#### Step 1
Acquire a memory lock for it

#### Step 2
Set current timestamp to `updatedAt`

#### Step 3
Update data in memory `${ModelName}:$store`

#### Step 4
Iterate fields in `${Model}.orderBy` and update the ranking only if `this.changed(field)` is `true`.
We use binary search for this step as well.

#### Step 5
Release the memory lock

## Memory Lock

### Config

I take the input design by [node-redlock](https://github.com/mike-marcacci/node-redlock)

```js
{
  // how many times the client will attempt to lock the resource before throwing an error
  retryCount: 5,

  // the time in ms between attempts
  retryDelay: 100, // ms

  // the max time in ms randomly added to retries
  retryJitter: 50  // ms
}
```

### Lock
#### Step 1
In a loop which will try `this.retryCount` at most to acquire the lock

#### Step 2
If it fails to acquire, it will sleep for `this.retryDelay + (Math.random() * this.retryJitter | 0)` ms and retry again

#### Step 3
after acquiring the lock, it will save a ttl record in `$lock:${resource}`

#### Step 4
return a lock

### Unlock
#### Step 1
Set `$lock:${resource}` to null

# IOC layer
I read [clean architecture](https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html) from uncle bob and one of the import rules is "dependency injection". It will be super easy to do unit test by using "dependency injection".

That's why I decide to use an IOC (Inversion of control) container to make things easier.

The IOC container lib I choose is https://github.com/jeffijoe/awilix

# CI-CD

I built a simple but efficient way with [Codeship](https://codeship.com/) and [Heroku](http://heroku.com/).
Every push to "Master" branch will automatically being test on [Codeship](https://codeship.com/) and being deployed to [Heroku](http://heroku.com/) after passing the test.
