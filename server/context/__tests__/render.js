const renderFactory = require('../render');

describe('render', () => {
  const ctx = {};

  beforeEach(() => {
    ctx.render = renderFactory();
  });

  test('success', () => {
    const status = 200;
    const body = 'body';

    ctx.render(status, body);

    expect(ctx.status).toBe(status);
    expect(ctx.body).toBe(body);
  });
});
