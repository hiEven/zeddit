module.exports = () => function render(status, body) {
  this.status = status;
  this.body = body;
};
