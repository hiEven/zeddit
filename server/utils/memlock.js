module.exports = ({
  storage,
}) => {
  function sleep(ms) {
    return new Promise((resolve) => {
      setTimeout(() => resolve(), ms);
    });
  }

  class Lock {
    constructor(store, resource) {
      if (!store) throw new Error('store required');
      if (!resource) throw new Error('resource required');

      this.store = store;
      this.resource = resource;
    }

    async isLocked() {
      return this.store[this.resource] !== null;
    }

    async unlock() {
      this.store[this.resource] = null;
    }
  }

  class Memlock {
    constructor(options = { retryCount: 5, retryDelay: 5, retryJitter: 2 }) {
      this.retryCount = options.retryCount;
      this.retryDelay = options.retryDelay; // ms
      this.retryJitter = options.retryJitter; // ms

      storage[this.constructor.getStoreKey()] = {};
    }

    static getStoreKey() {
      return '$lock';
    }

    static getStore() {
      return storage[this.getStoreKey()];
    }

    async lock(resource, ttl = 200) {
      const store = this.constructor.getStore();

      let canLock = false;
      for (let i = 0; i < this.retryCount; i++) {
        if (!store[resource] || new Date().getTime() - store[resource] >= 0) {
          store[resource] = +(new Date()) + ttl;
          canLock = true;
          break;
        }

        const delay = this.retryDelay + (Math.random() * this.retryJitter | 0);
        await sleep(delay);
      }

      if (!canLock) {
        throw new Error('fails to lock');
      }

      return new Lock(store, resource);
    }

    async isLocked(resource) {
      const store = this.constructor.getStore();

      return Boolean(store[resource] && (new Date().getTime() < store[resource]));
    }

    async waitUnlock(resource) {
      const store = this.constructor.getStore();

      if (await this.isLocked(resource)) {
        const remain = store[resource] - new Date().getTime();

        if (remain < 0) {
          return;
        }

        const throttle = remain / this.retryCount | 0;

        for (let i = 0; i < this.retryCount; i++) {
          await sleep(throttle + (Math.random * this.retryJitter | 0));

          const isLocked = await this.isLocked(resource);
          if (!isLocked) {
            break;
          }
        }
      }
    }
  }

  return Memlock;
};
