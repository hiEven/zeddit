const joi = require('joi');

module.exports = ({
  Promise,
}) => {
  joi.validate = Promise.promisify(joi.validate, joi);
  return joi;
};
