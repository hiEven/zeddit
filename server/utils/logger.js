const bunyan = require('bunyan');

module.exports = ({ config }) => {
  const logger = bunyan.createLogger({
    name: config.name,
    level: config.logger.level,
    serializers: bunyan.stdSerializers,
  });

  return logger;
};
