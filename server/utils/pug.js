const path = require('path');
const Pug = require('koa-pug');

module.exports = () => {
  const pug = new Pug({
    viewPath: path.resolve(__dirname, '../views'),
    basedir: 'path/for/pug/extends',
  });

  return pug;
};
