const MemormFactory = require('../memorm');

describe('Memorm', () => {
  const Joi = require('joi');
  const _ = require('lodash');
  const prefix = 'prefix';
  const storage = {};
  const mockMemlockInstance = {};
  const mockMemlock = jest.fn().mockImplementation(() => mockMemlockInstance);
  let mockSchema;
  let Memorm;
  let Model;
  let instance;

  beforeEach(() => {
    mockMemlockInstance.lock = jest.fn();
    mockMemlockInstance.waitUnlock = jest.fn();

    mockSchema = jest.fn();

    Memorm = MemormFactory({
      Joi,
      _,
      Memlock: mockMemlock,
      storage,
    });

    class extension extends Memorm {
      static get prefix() {
        return prefix;
      }

      static get schema() {
        return mockSchema();
      }
    }
    Model = extension;

    instance = Model.build({ id: '1', content: 'content', voteCount: 0 });
  });

  test('success', async () => {
    expect.assertions(3);

    try {
      Memorm.prefix; // eslint-disable-line
    } catch (err) {
      expect(err.message).toBe('prefix required');
    }

    try {
      Memorm.schema; // eslint-disable-line
    } catch (err) {
      expect(err.message).toBe('schema required');
    }

    expect(Memorm.orderBy).toEqual(['createdAt']);
  });

  test('getStoreKey', async () => {
    expect(Model.getStoreKey()).toBe(`${prefix}:$store`);
  });

  test('getStore', async () => {
    storage[Model.getStoreKey()] = 1;
    expect(Model.getStore()).toBe(1);
  });

  test('getOrderStoreKey', async () => {
    const field = 'field';
    expect(Model.getOrderStoreKey(field)).toBe(`${prefix}:$orderBy:${field}`);
  });

  test('getOrderStore', async () => {
    const field = 'field';
    storage[Model.getOrderStoreKey(field)] = 1;
    expect(Model.getOrderStore(field)).toBe(1);
  });

  test('lock', async () => {
    const resource = 'resource';
    const ttl = 'ttl';

    await Model.lock(resource, ttl);

    expect(mockMemlockInstance.lock.mock.calls.length).toBe(1);
    expect(mockMemlockInstance.lock.mock.calls[0]).toEqual([resource, ttl]);
  });

  test('waitUnlock', async () => {
    const resource = 'resource';

    await Model.waitUnlock(resource);

    expect(mockMemlockInstance.waitUnlock.mock.calls.length).toBe(1);
    expect(mockMemlockInstance.waitUnlock.mock.calls[0]).toEqual([resource]);
  });

  describe('_insertRank', async () => {
    test('success', async () => {
      const orderStore = [2, 3, 1];
      const store = {
        1: { voteCount: 1 },
        2: { voteCount: 2 },
        3: { voteCount: 3 },
      };

      Model.getOrderStore = jest.fn().mockReturnValue(orderStore);
      Model.getStore = jest.fn().mockReturnValue(store);

      Model._insertRank('voteCount', 3);

      expect(orderStore).toEqual([3, 2, 1]);
    });

    test('one item only', async () => {
      const orderStore = [1];
      const store = {
        1: { voteCount: 1 },
        2: { voteCount: 2 },
      };

      Model.getOrderStore = jest.fn().mockReturnValue(orderStore);
      Model.getStore = jest.fn().mockReturnValue(store);

      Model._insertRank('voteCount', 2);

      expect(orderStore).toEqual([2, 1]);
    });
  });

  test('syncStore', async () => {
    Model.syncStore();

    const storeKey = ['$store', '$orderBy:field', '$orderBy:createdAt'].map(key => `${prefix}:${key}`);

    storeKey.forEach((key) => {
      expect(storage[key]).not.toBeNull();
    });
  });

  test('build', async () => {
    mockSchema.mockReturnValue('qqqq');

    const attrs = {
      content: 'content',
    };

    const instantce = Model.build(attrs);

    expect(instantce.dataValue).toEqual(attrs);
    expect(instantce.$previous).toEqual(attrs);
  });

  describe('findAll', async () => {
    let store;

    beforeEach(async () => {
      store = {
        1: { id: 1, content: 'content1' },
        2: { id: 2, content: 'content2' },
        3: { id: 3, content: 'content3' },
      };

      Model.getStore = jest.fn().mockReturnValue(store);
      Model.build = jest.fn().mockImplementation(item => item);
    });

    test('success', async () => {
      const items = await Model.findAll();

      expect(items).toHaveLength(Object.keys(store).length);
      expect(items).toEqual(expect.arrayContaining(Object.values(store)));
    });

    test('order', async () => {
      const options = {
        order: 'createdAt',
      };

      const orderByCreatedAt = Object.keys(store).reverse();
      Model.getOrderStore = jest.fn().mockReturnValue(orderByCreatedAt);

      const items = await Model.findAll(options);

      expect(items).toHaveLength(Object.keys(store).length);
      items.forEach((item, index) => {
        expect(item.id).toEqual(+orderByCreatedAt[index]);
      });
    });

    test('offset', async () => {
      const options = { offset: 1 };
      const items = await Model.findAll(options);

      expect(items).toHaveLength(Object.keys(store).length - options.offset);
      expect(items).toEqual(expect.arrayContaining(Object.values(store).slice(1)));
    });

    test('limit', async () => {
      const options = { limit: 1 };
      const items = await Model.findAll(options);

      expect(items).toHaveLength(options.limit);
    });
  });

  test('findById', async () => {
    const store = {
      1: { id: 1, content: 'content' },
    };

    Model.getStore = jest.fn().mockReturnValue(store);

    const item = await Model.findById(1);
    expect(item).toEqual(store[1]);

    const notFound = await Model.findById(2);
    expect(notFound).not.toBeDefined();
  });

  test('create', async () => {
    const attrs = {
      content: 'content',
    };

    instance = {
      validate: jest.fn(),
      save: jest.fn(),
    };

    Model.build = jest.fn().mockReturnValue(instance);

    await Model.create(attrs);

    expect(Model.build.mock.calls.length).toBe(1);
    expect(Model.build.mock.calls[0]).toEqual([attrs]);

    expect(instance.validate.mock.calls.length).toBe(1);
    expect(instance.validate.mock.calls[0]).toEqual([]);

    expect(instance.save.mock.calls.length).toBe(1);
    expect(instance.save.mock.calls[0]).toEqual([]);
  });

  test('set', async () => {
    const key = 'content';
    const value = 'new content';
    const oldValue = instance.dataValue.content;

    instance.set(key, value);

    expect(instance.dataValue.content).toBe(value);
    expect(instance.$previous.content).toBe(oldValue);
  });

  test('get', async () => {
    expect(instance.get('content')).toBe('content');
  });

  test('changed', async () => {
    const key = 'content';
    const value = 'new content';

    instance.set(key, value);
    expect(instance.changed(key)).toBeTruthy();
  });

  test('toJSON', async () => {
    const json = { id: '1', content: 'content', voteCount: 0 };

    expect(instance.toJSON()).toEqual(json);
  });

  test('validate', async () => {
    const schema = 'schema';
    mockSchema.mockReturnValue(schema);

    const originalValidate = Joi.validate;
    Joi.validate = jest.fn().mockImplementation(value => value);

    await instance.validate();

    expect(Joi.validate.mock.calls).toHaveLength(1);
    expect(Joi.validate.mock.calls[0]).toEqual([
      instance.dataValue,
      schema,
    ]);

    Joi.validate = originalValidate;
  });

  test('save', async () => {
    const lock = { unlock: jest.fn() };
    Model.lock = jest.fn().mockReturnValue(lock);
    Model.getStore = jest.fn().mockReturnValue({});
    Model._insertRank = jest.fn();
    instance.set = jest.fn();
    instance.toJSON = jest.fn().mockReturnValue(instance.dataValue);

    await instance.save();

    expect(Model.lock.mock.calls).toHaveLength(1);
    expect(Model.lock.mock.calls[0]).toEqual([Model.prefix]);

    expect(instance.set.mock.calls).toHaveLength(2);
    expect(instance.set.mock.calls[0][0]).toEqual('createdAt');
    expect(instance.set.mock.calls[1][0]).toEqual('updatedAt');

    expect(Model.getStore.mock.calls).toHaveLength(1);
    expect(Model.getStore.mock.calls[0]).toEqual([]);

    expect(instance.toJSON.mock.calls).toHaveLength(1);
    expect(instance.toJSON.mock.calls[0]).toEqual([]);

    expect(Model._insertRank.mock.calls).toHaveLength(Model.orderBy.length);
    Model.orderBy.forEach((field, index) => {
      expect(Model._insertRank.mock.calls[index]).toEqual([field, instance.get('id')]);
    });

    expect(lock.unlock.mock.calls).toHaveLength(1);
    expect(lock.unlock.mock.calls[0]).toEqual([]);
  });

  describe('update', async () => {
    test('success by passing lock', async () => {
      Model.waitUnlock = jest.fn();
      Model.getStore = jest.fn().mockReturnValue({ [instance.dataValue.id]: {} });
      Model._insertRank = jest.fn();
      Model.lock = jest.fn();
      instance.get = jest.fn().mockImplementation((field) => { // eslint-disable-line
        if (field === 'id') return instance.dataValue.id;
      });
      instance.set = jest.fn();
      instance.toJSON = jest.fn();
      instance.changed = jest.fn().mockReturnValue(true);
      const lock = { unlock: jest.fn() };

      const attrs = { content: 'new content' };
      await instance.update(attrs, { lock });

      expect(Model.waitUnlock.mock.calls).toHaveLength(1);
      expect(Model.waitUnlock.mock.calls[0]).toEqual([Model.prefix]);

      expect(Model.lock.mock.calls).toHaveLength(0);

      expect(instance.set.mock.calls).toHaveLength(1 + Object.keys(attrs).length);
      expect(instance.set.mock.calls[0][0]).toEqual('updatedAt');
      expect(instance.set.mock.calls[1]).toEqual(['content', attrs.content]);

      expect(Model.getStore.mock.calls).toHaveLength(1);
      expect(Model.getStore.mock.calls[0]).toEqual([]);

      expect(instance.get.mock.calls).toHaveLength(1 + Model.orderBy.length);
      expect(instance.get.mock.calls[0]).toEqual(['id']);

      expect(instance.toJSON.mock.calls).toHaveLength(1);
      expect(instance.toJSON.mock.calls[0]).toEqual([]);

      expect(instance.changed.mock.calls).toHaveLength(Model.orderBy.length);
      expect(Model._insertRank.mock.calls).toHaveLength(Model.orderBy.length);
      Model.orderBy.forEach((field, index) => {
        expect(instance.changed.mock.calls[index]).toEqual([field]);
        expect(Model._insertRank.mock.calls[index]).toEqual([field, instance.dataValue.id]);
      });

      expect(lock.unlock.mock.calls).toHaveLength(1);
      expect(lock.unlock.mock.calls[0]).toEqual([]);
    });

    test('success by empty lock', async () => {
      Model.waitUnlock = jest.fn();
      Model.getStore = jest.fn().mockReturnValue({ [instance.dataValue.id]: {} });
      Model._insertRank = jest.fn();
      Model.lock = jest.fn().mockReturnValue({ unlock: jest.fn() });
      instance.get = jest.fn().mockImplementation((field) => { // eslint-disable-line
        if (field === 'id') return instance.dataValue.id;
      });
      instance.set = jest.fn();
      instance.toJSON = jest.fn();
      instance.changed = jest.fn().mockReturnValue(true);

      const attrs = { content: 'new content' };
      await instance.update(attrs);

      expect(Model.lock.mock.calls).toHaveLength(1);
      expect(Model.lock.mock.calls[0]).toEqual([instance.dataValue.id]);
    });
  });

  describe('increment', async () => {
    test('success', async () => {
      const lock = {
        isLocked: jest.fn().mockReturnValue(true),
        unlock: jest.fn(),
      };
      Model.lock = jest.fn().mockReturnValue(lock);
      Model.getStore = jest.fn().mockReturnValue({ [instance.dataValue.id]: instance.dataValue });
      instance.update = jest.fn();
      instance.get = jest.fn().mockImplementation((field) => { // eslint-disable-line
        if (field === 'id') return instance.dataValue.id;
      });

      const attrs = { voteCount: 1 };
      await instance.increment(attrs);

      expect(Model.lock.mock.calls).toHaveLength(1);
      expect(Model.lock.mock.calls[0]).toEqual([instance.dataValue.id]);

      expect(Model.getStore.mock.calls).toHaveLength(1);
      expect(Model.getStore.mock.calls[0]).toEqual([]);

      expect(instance.get.mock.calls).toHaveLength(2);
      expect(instance.get.mock.calls[0]).toEqual(['id']);

      expect(instance.update.mock.calls).toHaveLength(1);
      expect(instance.update.mock.calls[0]).toEqual([attrs, { lock }]);

      expect(lock.isLocked.mock.calls).toHaveLength(1);
      expect(lock.isLocked.mock.calls[0]).toEqual([]);

      expect(lock.unlock.mock.calls).toHaveLength(1);
      expect(lock.unlock.mock.calls[0]).toEqual([]);
    });
  });
});
