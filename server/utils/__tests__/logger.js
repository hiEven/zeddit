const loggerFactory = require('../logger');

describe('logger', () => {
  let logger;

  beforeEach(() => {
    logger = loggerFactory({
      config: {
        name: 'name',
        logger: {
          level: 'debug',
        },
      },
    });
  });

  test('success', async () => {

    expect(logger.info).not.toBeNull();
    expect(logger.warn).not.toBeNull();
    expect(logger.erro).not.toBeNull();
  });
});
