const MemlockFactory = require('../memlock');

describe('memlock', () => {
  let Memlock;
  let memlock;
  const storage = { $lock: {} };

  beforeEach(() => {
    Memlock = MemlockFactory({ storage });
    memlock = new Memlock();
  });

  test('success', async () => {
    expect(memlock.retryCount).toBe(5);
    expect(memlock.retryDelay).toBe(5);
    expect(memlock.retryJitter).toBe(2);
  });

  test('getStoreKey', async () => {
    expect(Memlock.getStoreKey()).toBe('$lock');
  });

  test('getStore', async () => {
    storage.$lock = 1;
    expect(Memlock.getStore()).toEqual(storage.$lock);
  });

  describe('lock', async () => {
    const resource = 'resource';

    test('success', async () => {
      const lock = await memlock.lock(resource);

      expect(storage.$lock[resource]).not.toBeNull();
      expect(lock.resource).toBe(resource);
    });

    describe('lock instance', async () => {
      test('isLocked', async () => {
        const lock = await memlock.lock(resource);

        expect(await lock.isLocked()).toBeTruthy();
      });

      test('unlock', async () => {
        const lock = await memlock.lock(resource);

        await lock.unlock();

        expect(await lock.isLocked()).toBeFalsy();
      });
    });

    // TODO: enhance test
    test('retry', async () => {
      storage.$lock[resource] = new Date().getTime() + 5;

      await memlock.lock(resource);
    });

    test('fails to lock', async () => {
      expect.assertions(1);
      memlock.retryCount = 0;

      try {
        await memlock.lock(resource);
      } catch (err) {
        expect(err.message).toBe('fails to lock');
      }
    });
  });

  describe('isLocked', async () => {
    const resource = 'resource';

    test('lock', async () => {
      storage.$lock = { [resource]: new Date().getTime() + 10000 };

      expect(await memlock.isLocked(resource)).toBeTruthy();
    });

    test('unlock', async () => {
      expect(await memlock.isLocked(resource)).toBeFalsy();
    });

    test('lock expired', async () => {
      storage.$lock = { [resource]: new Date().getTime() - 10000 };

      expect(await memlock.isLocked(resource)).toBeFalsy();
    });
  });

  // TODO: enhance test
  describe('waitUnlock', async () => {
    const resource = 'resource';

    test('not lock', async () => {
      await memlock.waitUnlock(resource);
    });

    test('wait', async () => {
      storage.$lock[resource] = new Date().getTime() + 20;

      await memlock.waitUnlock(resource);
    });
  });
});
