const originalJoi = require('joi');
const JoiFactory = require('../joi');

const Promise = require('bluebird');

describe('joi', () => {
  let joi;

  beforeEach(() => {
    joi = JoiFactory({
      Promise,
    });
  });

  test('success', async () => {
    const schema = originalJoi.string();

    const form = 'string';
    const result = await joi.validate(form, schema);
    expect(result).toEqual(form);
  });
});
