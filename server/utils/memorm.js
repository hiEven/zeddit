module.exports = ({
  Joi,
  _: { isNumber },
  Memlock,
  storage,
}) => {
  const memlock = new Memlock();

  class Memorm {
    static get prefix() {
      throw new Error('prefix required');
    }

    static get schema() {
      throw new Error('schema required');
    }

    static get orderBy() {
      return ['createdAt'];
    }

    static getStoreKey() {
      return `${this.prefix}:$store`;
    }

    static getStore() {
      return storage[this.getStoreKey()];
    }

    static getOrderStoreKey(field) {
      return `${this.prefix}:$orderBy:${field}`;
    }

    static getOrderStore(field) {
      return storage[this.getOrderStoreKey(field)];
    }

    static async lock(resource, ttl = 10) {
      return memlock.lock(resource, ttl);
    }

    static async waitUnlock(resource) {
      return memlock.waitUnlock(resource);
    }

    static _insertRank(field, id) {
      const orderStore = this.getOrderStore(field);
      const store = this.getStore();
      const target = store[id];

      // TODO: use binary search in the future
      // Remove
      // Time Complexity: O(n)
      //
      // I decide to use for-loop for removing one element because when there are multiple items
      // with same value, the binary search would be complex.
      // e.g.
      // [a, b, c, d, e, f]
      // the a~f are ids and their value are all "2"
      // when we need to remove "f", it would be complex for the cursor in binary search.
      // Which direction should the cursor move toward to? left or right?
      // Ideally, we can do two-way searching and it can still reduce the time complexity
      // However, I decide not to overengineer.
      // Only refactor this when we do encounter performance issue.
      const index = orderStore.indexOf(id);
      if (index !== -1) {
        orderStore.splice(index, 1);
      }

      // Insert
      // Binary search
      // Time Complexity: O(logn)
      let left = 0;
      let right = orderStore.length - 1;
      if (orderStore.length === 1) {
        left = store[orderStore[0]][field] > target[field] ? 1 : 0;
      } else {
        while (left < right) {
          const mid = (left + right) / 2 | 0; // eslint-disable-line
          const tmp = store[orderStore[mid]];

          if (tmp[field] > target[field]) {
            left = mid + 1;
          } else if (tmp[field] < target[field]) {
            right = mid;
          } else {
            left = mid;
            break;
          }
        }
      }
      orderStore.splice(left, 0, id);
    }

    static syncStore() {
      storage[this.getStoreKey()] = {};

      this.orderBy.forEach((field) => {
        storage[this.getOrderStoreKey(field)] = [];
      });
    }

    static build(attrs = {}) {
      const instance = new this(attrs);
      instance.dataValue = {};

      Object.keys(attrs).forEach((key) => {
        instance.dataValue[key] = attrs[key];
      });

      instance.$previous = Object.assign({}, instance.dataValue);

      return instance;
    }

    static async findAll(options = {}) {
      const store = this.getStore();
      let keys = Object.keys(store);

      if (options.order && this.orderBy.includes(options.order)) {
        keys = this.getOrderStore(options.order);
      }

      if (options.offset) {
        keys.splice(0, options.offset);
      }

      if (options.limit) {
        keys = keys.slice(0, options.limit);
      }

      const items = [];
      keys.forEach((key) => {
        items.push(this.build(store[key]));
      });

      return items;
    }

    static async findById(id) {
      const store = this.getStore();

      return store[id];
    }

    static async create(attrs) {
      const instance = this.build(attrs);
      await instance.validate();
      return instance.save();
    }

    set(key, value) {
      this.$previous[key] = this.dataValue[key];
      this.dataValue[key] = value;
    }

    get(key) {
      return this.dataValue[key];
    }

    changed(key) {
      return this.get(key) !== this.$previous[key];
    }

    toJSON() {
      return this.dataValue;
    }

    async validate() {
      this.dataValue = await Joi.validate(this.dataValue, this.constructor.schema);
    }

    async save() {
      // lock table
      const lock = await this.constructor.lock(this.constructor.prefix);

      this.set('createdAt', new Date());
      this.set('updatedAt', this.get('createdAt'));

      // normal storage
      const store = this.constructor.getStore();
      store[this.get('id')] = this.toJSON();

      // order reference
      this.constructor.orderBy.forEach((field) => {
        this.constructor._insertRank(field, this.get('id'));
      });

      await lock.unlock();
      return this;
    }

    async update(attrs, options = {}) {
      // wait for table unlocked
      await this.constructor.waitUnlock(this.constructor.prefix);

      let { lock } = options;
      if (!lock) {
        // lock row
        lock = await this.constructor.lock(this.get('id'));
      }

      this.set('updatedAt', new Date());

      Object.keys(attrs).forEach((key) => {
        this.set(key, attrs[key]);
      });

      const store = this.constructor.getStore();
      Object.assign(store[this.get('id')], this.toJSON());

      this.constructor.orderBy.forEach((field) => {
        if (this.changed(field)) {
          this.constructor._insertRank(field, this.get('id')); // eslint-disable-line
        }
      });

      await lock.unlock();
      return this;
    }

    async increment(attrs) {
      // lock row
      const lock = await this.constructor.lock(this.get('id'));
      const store = this.constructor.getStore();
      const previous = store[this.get('id')];

      Object.keys(attrs).forEach((key) => {
        if (isNumber(previous[key]) && isNumber(attrs[key])) {
          attrs[key] = previous[key] + attrs[key];  // eslint-disable-line
        }
      });

      await this.update(attrs, { lock });

      if (await lock.isLocked()) {
        await lock.unlock();
      }

      return this;
    }
  }

  return Memorm;
};
