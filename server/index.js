const container = require('./container');

const config = container.resolve('config');
const app = container.resolve('server');

const logger = container.resolve('logger');

app.listen(config.server.port, () => {
  logger.info(`server is listening on ${config.server.port}`);
});
