const ErrorCode = require('../ErrorCode');
const AppError = require('../AppError');

describe('AppError', () => {
  test('success', () => {
    const error = new AppError();

    expect(error.name).toBe('AppError');
    expect(error.isClientError).toBeTruthy();
    expect(error.status).toBe(500);
    expect(error.body).toEqual({
      message: 'Internal server error',
      error: ErrorCode.Unknown,
    });
  });

  test('Error.captureStackTrace is unavailable', () => {
    const originalCaptureStackTrace = Error.captureStackTrace;
    Error.captureStackTrace = null;

    const error = new AppError();
    expect(error.stack).not.toBeNull();

    Error.captureStackTrace = originalCaptureStackTrace;
  });

  test('body.description', () => {
    const body = {
      description: 'description',
    };

    const error = new AppError(500, body);

    expect(error.body).toEqual(body);
  });
});
