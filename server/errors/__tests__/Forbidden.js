const ErrorCode = require('../ErrorCode');
const {
  Forbidden,
  InvalidCSRFToken,
} = require('../Forbidden');

describe('Forbidden', () => {
  test('success', () => {
    const error = new Forbidden();

    expect(error.name).toBe('Forbidden');
    expect(error.isClientError).toBeTruthy();
    expect(error.status).toBe(403);
    expect(error.body).toEqual({
      message: 'Forbidden',
      error: ErrorCode.Forbidden,
    });
  });
});

describe('InvalidCSRFToken', () => {
  test('success', () => {
    const error = new InvalidCSRFToken();

    expect(error.name).toBe('InvalidCSRFToken');
    expect(error.isClientError).toBeTruthy();
    expect(error.status).toBe(403);
    expect(error.body).toEqual({
      message: 'Invalid CSRF token',
      error: ErrorCode.InvalidCSRFToken,
    });
  });
});
