const ErrorCode = require('../ErrorCode');
const {
  NotFound,
  TopicNotFound,
} = require('../NotFound');

describe('NotFound', () => {
  test('success', () => {
    const error = new NotFound();

    expect(error.name).toBe('NotFound');
    expect(error.isClientError).toBeTruthy();
    expect(error.status).toBe(404);
    expect(error.body).toEqual({
      message: 'Not found',
      error: ErrorCode.NotFound,
    });
  });
});

describe('TopicNotFound', () => {
  test('success', () => {
    const error = new TopicNotFound();

    expect(error.name).toBe('TopicNotFound');
    expect(error.isClientError).toBeTruthy();
    expect(error.status).toBe(404);
    expect(error.body).toEqual({
      message: 'Topic not found',
      error: ErrorCode.TopicNotFound,
    });
  });
});
