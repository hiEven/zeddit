const ErrorCode = require('./ErrorCode');
const AppError = require('./AppError');

class NotFound extends AppError {
  constructor(message = 'Not found', error = ErrorCode.NotFound) {
    super(404, { message, error });
  }
}

class TopicNotFound extends NotFound {
  constructor(message = 'Topic not found', error = ErrorCode.TopicNotFound) {
    super(message, error);
  }
}

module.exports = {
  NotFound,
  TopicNotFound,
};
