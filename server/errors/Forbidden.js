const ErrorCode = require('./ErrorCode');
const AppError = require('./AppError');

class Forbidden extends AppError {
  constructor(message = 'Forbidden', error = ErrorCode.Forbidden) {
    super(403, { message, error });
  }
}

class InvalidCSRFToken extends Forbidden {
  constructor(message = 'Invalid CSRF token', error = ErrorCode.InvalidCSRFToken) {
    super(message, error);
  }
}

module.exports = {
  Forbidden,
  InvalidCSRFToken,
};
