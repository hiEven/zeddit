module.exports = ({
  Unknown: 1000,

  // 400
  BadRequest: 1100,

  // 401
  Unauthorized: 1200,

  // 403
  Forbidden: 1300,
  InvalidCSRFToken: 1301,

  // 404
  NotFound: 1400,
  TopicNotFound: 1401,

  // 429
  TooManyRequests: 1500,
});
