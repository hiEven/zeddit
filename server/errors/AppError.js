const ErrorCode = require('./ErrorCode');

const defaultBody = {
  message: 'Internal server error',
  error: ErrorCode.Unknown,
};

class AppError extends Error {
  constructor(status = 500, body = defaultBody) {
    super(body.message || body.description);

    this.name = this.constructor.name;
    this.isClientError = !!body.message;
    this.status = status;
    this.body = body;

    if (typeof Error.captureStackTrace === 'function') {
      Error.captureStackTrace(this, this.constructor);
    } else {
      this.stack = (new Error(body.message || body.description)).stack;
    }
  }
}

module.exports = AppError;
