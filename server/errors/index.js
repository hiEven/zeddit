const { merge } = require('lodash');

module.exports = merge({
  ErrorCode: require('./ErrorCode'),
  AppError: require('./AppError'),
}, require('./Forbidden'), require('./NotFound'));
