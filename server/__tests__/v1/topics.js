const supertest = require('supertest');

const container = require('../../container');

describe('topics', async () => {
  let server;
  let request;

  beforeAll(async () => {
    server = container.resolve('server').listen();
    request = supertest(server);
  });

  afterAll(async () => {
    server.close();
  });

  describe('list', async () => {
    test('success', async () => {
      const Topic = container.resolve('Topic');

      await Topic.create({ content: '1' });

      const res = await request.get('/v1/topics')
        .expect(200);

      expect(res.body).toHaveLength(1);
      expect(res.body[0].content).toEqual('1');
    });
  });
});
