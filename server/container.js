const awilix = require('awilix');

const container = awilix.createContainer();

container
  .registerValue({
    env: process.env.NODE_ENV,
    storage: {},
    Errors: require('./errors'),

    // node_modules
    uuidV4: require('uuid/v4'),
    Promise: require('bluebird'),
    CSRF: require('koa-csrf'),
    _: require('lodash'),
  })
  .registerFunction({
    config: [require('../config'), { lifetime: awilix.Lifetime.SINGLETON }],

    // server
    server: [require('./server'), { lifetime: awilix.Lifetime.SINGLETON }],
    routes: [require('./routes'), { lifetime: awilix.Lifetime.SINGLETON }],
    render: [require('./context/render'), { lifetime: awilix.Lifetime.SINGLETON }],

    // utils
    logger: [require('./utils/logger'), { lifetime: awilix.Lifetime.SINGLETON }],
    pug: [require('./utils/pug'), { lifetime: awilix.Lifetime.SINGLETON }],
    Joi: [require('./utils/joi'), { lifetime: awilix.Lifetime.SINGLETON }],
    Memorm: [require('./utils/memorm'), { lifetime: awilix.Lifetime.SINGLETON }],
    Memlock: require('./utils/memlock', { lifetime: awilix.Lifetime.SINGLETON }),

    // models
    Topic: [require('./models/Topic'), { lifetime: awilix.Lifetime.SINGLETON }],

    // middlewares
    apiLogger: [require('./routes/middlewares/apiLogger'), { lifetime: awilix.Lifetime.SINGLETON }],
    errorRecover: [require('./routes/middlewares/errorRecover'), { lifetime: awilix.Lifetime.SINGLETON }],
    csrf: [require('./routes/middlewares/csrf'), { lifetime: awilix.Lifetime.SINGLETON }],
    checkTopicExist: [require('./routes/middlewares/checkTopicExist'), { lifetime: awilix.Lifetime.SINGLETON }],

    // forms
    listTopicSchema: [require('./routes/v1/forms/topics/list'), { lifetime: awilix.Lifetime.SINGLETON }],
    createTopicSchema: [require('./routes/v1/forms/topics/create'), { lifetime: awilix.Lifetime.SINGLETON }],

    // routes
    v1: [require('./routes/v1'), { lifetime: awilix.Lifetime.SINGLETON }],
    v1TopicList: [require('./routes/v1/topics/list'), { lifetime: awilix.Lifetime.SINGLETON }],
    v1TopicCreate: [require('./routes/v1/topics/create'), { lifetime: awilix.Lifetime.SINGLETON }],
    v1TopicUpvote: [require('./routes/v1/topics/upvote'), { lifetime: awilix.Lifetime.SINGLETON }],
    v1TopicDownvote: [require('./routes/v1/topics/downvote'), { lifetime: awilix.Lifetime.SINGLETON }],

    // services
    createTopicService: [require('./services/topics/create'), { lifetime: awilix.Lifetime.SINGLETON }],
    listTopicService: [require('./services/topics/list'), { lifetime: awilix.Lifetime.SINGLETON }],
    incrementTopicByIdService: [require('./services/topics/increment'), { lifetime: awilix.Lifetime.SINGLETON }],
    findTopicByIdService: [require('./services/topics/findById'), { lifetime: awilix.Lifetime.SINGLETON }],
    formatTopic: [require('./services/topics/format'), { lifetime: awilix.Lifetime.SINGLETON }],
    formatTopicList: [require('./services/topics/formatList'), { lifetime: awilix.Lifetime.SINGLETON }],
  });

module.exports = container;
