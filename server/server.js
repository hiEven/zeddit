const Koa = require('koa');
const session = require('koa-session');

const app = new Koa();

module.exports = ({
  routes,
  render,
}) => {
  app.context.render = render;
  app.keys = ['a', 'b'];

  app.use(session(app));
  app.use(routes);

  return app;
};
