const path = require('path');
const koaBody = require('koa-body');
const router = require('koa-router')();
const serve = require('koa-static');

module.exports = ({
  apiLogger,
  errorRecover,
  csrf,
  v1,
  storage,
  pug,
}) => {
  router.use(apiLogger());
  router.use(errorRecover());
  router.use(koaBody());
  router.use(serve(path.resolve(__dirname, '../../client/public')));
  router.use(csrf());

  router.get('/mem', async (ctx) => {
    ctx.render(200, storage);
  });

  router.use('/v1', v1);

  router.get('*', async (ctx) => {
    ctx.render(200, pug.render('index', { $STATE: JSON.stringify({ app: { csrf: ctx.csrf } }) }));
  });

  return router.routes();
};
