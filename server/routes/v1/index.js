const router = require('koa-router')();

module.exports = ({
  checkTopicExist,
  v1TopicList,
  v1TopicCreate,
  v1TopicUpvote,
  v1TopicDownvote,
}) => {
  router.get('/', async (ctx) => {
    ctx.render(200, { status: 'ok' });
  });
  router.get('/topics', v1TopicList);
  router.post('/topics', v1TopicCreate);
  router.post('/topics/:topicId/upvote', checkTopicExist(), v1TopicUpvote);
  router.post('/topics/:topicId/downvote', checkTopicExist(), v1TopicDownvote);

  return router.routes();
};
