module.exports = ({
  Joi,
}) => (
  Joi.object().keys({
    limit: Joi.number().integer().max(100).min(1).default(20), // eslint-disable-line
    order: Joi.string().valid(['voteCount', 'createdAt']).default('voteCount'),
  })
);
