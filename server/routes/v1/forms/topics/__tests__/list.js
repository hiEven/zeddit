const listFactory = require('../list');

const Joi = require('../../../../../utils/joi')(require('bluebird'));

describe('list', async () => {
  let list;

  beforeEach(async () => {
    list = listFactory({ Joi });
  });

  const tests = [
    {
      description: 'success',
      form: {},
      expectedForm: {
        limit: 20,
        order: 'voteCount',
      },
    },
    {
      description: '`limit` should be string',
      form: { limit: 'non number' },
    },
    // TODO: enhance test
  ];

  tests.forEach((t) => {
    test(t.description, async () => {
      if (t.expectedForm !== undefined) {
        const result = await Joi.validate(t.form, list);
        expect(result).toEqual(t.expectedForm);
      } else {
        expect.assertions(1);
        try {
          await Joi.validate(t.form, list);
        } catch (err) {
          expect(err.isJoi).toBeTruthy();
        }
      }
    });
  });
});
