const createFactory = require('../create');

const Joi = require('../../../../../utils/joi')(require('bluebird'));

describe('create', async () => {
  let create;

  beforeEach(async () => {
    create = createFactory({ Joi });
  });

  const tests = [
    {
      description: 'success',
      form: { content: 'content' },
      expectedForm: {
        content: 'content',
      },
    },
    {
      description: 'should be string',
      form: { content: 1 },
    },
    {
      description: 'content required',
      form: {},
    },
    {
      description: 'content can\'t be greater than 255',
      form: { content: Array(256).fill('a').join('') },
    },
  ];

  tests.forEach((t) => {
    test(t.description, async () => {
      if (t.expectedForm !== undefined) {
        const result = await Joi.validate(t.form, create);
        expect(result).toEqual(t.expectedForm);
      } else {
        expect.assertions(1);
        try {
          await Joi.validate(t.form, create);
        } catch (err) {
          expect(err.isJoi).toBeTruthy();
        }
      }
    });
  });
});
