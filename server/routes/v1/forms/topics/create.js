module.exports = ({
  Joi,
}) => (
  Joi.object({
    content: Joi.string().max(255).trim().required(),
  })
);
