module.exports = ({
  incrementTopicByIdService,
}) => async (ctx) => {
  const { topicId } = ctx.params;

  const topic = await incrementTopicByIdService(topicId, {
    voteCount: 1,
  });

  ctx.render(200, topic);
};
