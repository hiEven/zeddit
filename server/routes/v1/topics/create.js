module.exports = ({
  Joi,
  createTopicSchema,
  createTopicService,
  formatTopic,
}) => async (ctx) => {
  const form = await Joi.validate(ctx.request.body, createTopicSchema);

  const topic = await createTopicService(form);

  ctx.render(201, await formatTopic(topic));
};
