module.exports = ({
  Joi,
  listTopicSchema,
  listTopicService,
  formatTopicList,
}) => async (ctx) => {
  const form = await Joi.validate(ctx.request.query, listTopicSchema);

  const topics = await listTopicService(form);

  ctx.render(200, await formatTopicList(topics));
};
