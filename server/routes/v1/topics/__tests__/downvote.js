const downvoteFactory = require('../downvote');

describe('downvote', async () => {
  let downvote;
  let mockIncrementTopicByIdService;
  let mockContext;

  beforeEach(async () => {
    mockIncrementTopicByIdService = jest.fn();
    mockContext = { render: jest.fn() };

    downvote = downvoteFactory({
      incrementTopicByIdService: mockIncrementTopicByIdService,
    });
  });

  test('success', async () => {
    mockContext.params = {
      topicId: 'topci id',
    };

    await downvote(mockContext);

    expect(mockIncrementTopicByIdService.mock.calls.length).toBe(1);
    expect(mockIncrementTopicByIdService.mock.calls[0]).toEqual([
      mockContext.params.topicId,
      { voteCount: -1 },
    ]);

    expect(mockContext.render.mock.calls.length).toBe(1);
    expect(mockContext.render.mock.calls[0][0]).toEqual(200);
  });
});
