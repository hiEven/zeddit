const createFactory = require('../create');

const createTopicSchema = require('../../forms/topics/create');

describe('create', async () => {
  let create;
  let mockJoi;
  let mockCreateTopciService;
  let mockFormatTopic;
  let mockContext;

  beforeEach(async () => {
    mockJoi = { validate: jest.fn() };
    mockCreateTopciService = jest.fn();
    mockFormatTopic = jest.fn();
    mockContext = { render: jest.fn() };

    create = createFactory({
      Joi: mockJoi,
      createTopicSchema,
      createTopicService: mockCreateTopciService,
      formatTopic: mockFormatTopic,
    });
  });

  test('success', async () => {
    mockContext.request = {
      body: 'body',
    };

    const expectedJoiReturnValue = 'value';
    mockJoi.validate.mockReturnValue(expectedJoiReturnValue);

    const expectedServiceReturnValue = 'value';
    mockCreateTopciService.mockReturnValue(expectedServiceReturnValue);

    const expectedFormatReturnValue = 'value';
    mockFormatTopic.mockReturnValue(expectedFormatReturnValue);

    await create(mockContext);

    expect(mockJoi.validate.mock.calls.length).toBe(1);
    expect(mockJoi.validate.mock.calls[0]).toEqual([mockContext.request.body, createTopicSchema]);

    expect(mockCreateTopciService.mock.calls.length).toBe(1);
    expect(mockCreateTopciService.mock.calls[0]).toEqual([expectedJoiReturnValue]);

    expect(mockFormatTopic.mock.calls.length).toBe(1);
    expect(mockFormatTopic.mock.calls[0]).toEqual([expectedServiceReturnValue]);

    expect(mockContext.render.mock.calls.length).toBe(1);
    expect(mockContext.render.mock.calls[0]).toEqual([201, expectedFormatReturnValue]);
  });
});
