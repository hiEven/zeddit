const listFactory = require('../list');

const listTopicSchema = require('../../forms/topics/list');

describe('list', async () => {
  let list;
  let mockJoi;
  let mockListTopicService;
  let mockFormatTopic;
  let mockContext;

  beforeEach(async () => {
    mockJoi = { validate: jest.fn() };
    mockListTopicService = jest.fn();
    mockFormatTopic = jest.fn();
    mockContext = { render: jest.fn() };

    list = listFactory({
      Joi: mockJoi,
      listTopicSchema,
      listTopicService: mockListTopicService,
      formatTopicList: mockFormatTopic,
    });
  });

  test('success', async () => {
    mockContext.request = {
      query: 'query',
    };

    const expectedJoiReturnValue = 'value';
    mockJoi.validate.mockReturnValue(expectedJoiReturnValue);

    const expectedServiceReturnValue = 'value';
    mockListTopicService.mockReturnValue(expectedServiceReturnValue);

    const expectedFormatReturnValue = 'value';
    mockFormatTopic.mockReturnValue(expectedFormatReturnValue);

    await list(mockContext);

    expect(mockJoi.validate.mock.calls.length).toBe(1);
    expect(mockJoi.validate.mock.calls[0]).toEqual([mockContext.request.query, listTopicSchema]);

    expect(mockListTopicService.mock.calls.length).toBe(1);
    expect(mockListTopicService.mock.calls[0]).toEqual([expectedJoiReturnValue]);

    expect(mockFormatTopic.mock.calls.length).toBe(1);
    expect(mockFormatTopic.mock.calls[0]).toEqual([expectedServiceReturnValue]);

    expect(mockContext.render.mock.calls.length).toBe(1);
    expect(mockContext.render.mock.calls[0]).toEqual([200, expectedFormatReturnValue]);
  });
});
