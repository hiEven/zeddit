module.exports = ({
  logger,
}) => () => async (ctx, next) => {
  try {
    await next();
    logger.info({
      req: ctx.request,
      res: ctx.response,
    });
  } catch (err) {
    const data = {
      req: ctx.request,
      res: ctx.response,
      err,
    };

    if (err.isClientError) {
      logger.warn(data);
    } else {
      logger.error(data);
    }
  }
};
