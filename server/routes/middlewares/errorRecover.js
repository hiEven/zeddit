module.exports = ({
  Errors,
}) => {
  function handleClientError(ctx, err) {
    const { status, body } = err;

    ctx.render(status, body);
  }

  function handleJoiError(ctx, err) {
    const detail = err.details[0];
    const { message, path } = detail;

    ctx.render(400, {
      message,
      field: path,
      error: Errors.ErrorCode.BadRequest,
    });
  }

  function handleUnknownError(ctx) {
    ctx.render(500, {
      error: Errors.ErrorCode.Unknown,
      message: 'Internal server error',
    });
  }

  return () => async (ctx, next) => {
    try {
      await next();
    } catch (err) {
      if (err.isClientError) {
        handleClientError(ctx, err);
      } else if (err.isJoi) {
        handleJoiError(ctx, err);
      } else {
        handleUnknownError(ctx, err);
      }

      throw err;
    }
  };
};
