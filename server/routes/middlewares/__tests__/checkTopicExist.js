const checkTopicExistFactory = require('../checkTopicExist');

const Errors = require('../../../errors');
const Joi = require('joi');

describe('apiLogger', () => {
  let checkTopicExist;
  let mockFindTopicByIdService;

  beforeEach(() => {
    mockFindTopicByIdService = jest.fn();

    checkTopicExist = checkTopicExistFactory({
      Errors,
      Joi,
      findTopicByIdService: mockFindTopicByIdService,
    });
  });

  test('success', async () => {
    const mockContext = {
      params: {
        topicId: 'topicId',
      },
    };
    const mockNext = jest.fn();
    Joi.validate = jest.fn();
    mockFindTopicByIdService.mockReturnValue({});

    await checkTopicExist()(mockContext, mockNext);

    expect(Joi.validate.mock.calls.length).toBe(1);
    expect(Joi.validate.mock.calls[0][0]).toEqual(mockContext.params.topicId);
    expect(mockFindTopicByIdService.mock.calls.length).toBe(1);
    expect(mockFindTopicByIdService.mock.calls[0][0]).toEqual(mockContext.params.topicId);
    expect(mockNext.mock.calls.length).toBe(1);
  });

  test('topic doesn\'t exist', async () => {
    expect.assertions(2);

    const mockContext = {
      params: {
        topicId: 'topicId',
      },
    };
    const mockNext = jest.fn();
    Joi.validate = jest.fn();

    try {
      await checkTopicExist()(mockContext, mockNext);
    } catch (err) {
      expect(err instanceof Errors.TopicNotFound).toBeTruthy();
    }

    expect(mockNext.mock.calls.length).toBe(0);
  });
});
