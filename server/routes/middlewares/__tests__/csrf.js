const csrfFactory = require('../csrf');

const Errors = require('../../../errors');

describe('csrf', () => {
  let csrf;
  let mockCSRFLib;
  let CSRFInstance;

  beforeEach(() => {
    CSRFInstance = jest.fn();
    mockCSRFLib = jest.fn().mockImplementation(() => CSRFInstance);

    csrf = csrfFactory({
      CSRF: mockCSRFLib,
      Errors,
    });
  });

  test('success', async () => {
    const mockContext = { context: 'context' };
    const mockNext = { next: 'next' };

    await csrf()(mockContext, mockNext);

    expect(mockCSRFLib.mock.calls.length).toBe(1);
    expect(CSRFInstance.mock.calls.length).toBe(1);
    expect(CSRFInstance.mock.calls[0]).toEqual([
      mockContext,
      mockNext,
    ]);
  });

  test('throw CSRF error', async () => {
    expect.assertions(1);
    const mockContext = { context: 'context' };
    const mockNext = { next: 'next' };

    CSRFInstance.mockImplementation(() => { throw new Error('Invalid CSRF token'); });

    try {
      await csrf()(mockContext, mockNext);
    } catch (err) {
      expect(err instanceof Errors.InvalidCSRFToken).toBeTruthy();
    }
  });

  test('throw other error', async () => {
    expect.assertions(1);
    const mockContext = { context: 'context' };
    const mockNext = { next: 'next' };

    CSRFInstance.mockImplementation(() => { throw new Error(''); });

    try {
      await csrf()(mockContext, mockNext);
    } catch (err) {
      expect(err instanceof Errors.InvalidCSRFToken).toBeFalsy();
    }
  });
});
