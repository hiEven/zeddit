const errorRecoverFactory = require('../errorRecover');

const Errors = require('../../../errors');

describe('errorRecover', () => {
  let errorRecover;
  let mockContext;
  let mockNext;

  beforeEach(() => {
    mockContext = {
      render: jest.fn(),
    };
    mockNext = jest.fn();

    errorRecover = errorRecoverFactory({
      Errors,
    });
  });

  test('success', async () => {
    await errorRecover()(mockContext, mockNext);

    expect(mockNext.mock.calls.length).toBe(1);
  });

  test('handle client error', async () => {
    expect.assertions(2);
    const expectedError = new Errors.AppError();
    mockNext.mockImplementation(() => { throw expectedError; });

    try {
      await errorRecover()(mockContext, mockNext);
    } catch (err) {
      expect(mockContext.render.mock.calls.length).toBe(1);
      expect(mockContext.render.mock.calls[0]).toEqual([
        expectedError.status,
        expectedError.body,
      ]);
    }
  });

  test('handle Joi error', async () => {
    expect.assertions(2);
    const Joi = require('../../../utils/joi')({ Promise: require('bluebird') });
    let joiError;

    try {
      await Joi.validate('a', Joi.object().keys({ content: Joi.string() }));
    } catch (err) {
      joiError = err;
    }

    mockNext.mockImplementation(() => { throw joiError; });

    try {
      await errorRecover()(mockContext, mockNext);
    } catch (err) {
      expect(mockContext.render.mock.calls.length).toBe(1);
      expect(mockContext.render.mock.calls[0]).toEqual([
        400,
        {
          message: joiError.details[0].message,
          field: joiError.details[0].path,
          error: Errors.ErrorCode.BadRequest,
        },
      ]);
    }
  });

  test('handle unknown error', async () => {
    expect.assertions(2);
    const expectedError = new Error();
    mockNext.mockImplementation(() => { throw expectedError; });

    try {
      await errorRecover()(mockContext, mockNext);
    } catch (err) {
      expect(mockContext.render.mock.calls.length).toBe(1);
      expect(mockContext.render.mock.calls[0]).toEqual([
        500,
        {
          error: Errors.ErrorCode.Unknown,
          message: 'Internal server error',
        },
      ]);
    }
  });
});
