const AppError = require('../../../errors/AppError');
const apiLoggerFactory = require('../apiLogger');

describe('apiLogger', () => {
  let apiLogger;
  let logger;

  beforeEach(() => {
    logger = {
      info: jest.fn(),
      error: jest.fn(),
      warn: jest.fn(),
    };

    apiLogger = apiLoggerFactory({ logger });
  });

  test('success', async () => {
    const mockContext = {
      request: 'request',
      response: 'response',
    };
    const mockNext = jest.fn();

    await apiLogger()(mockContext, mockNext);

    expect(mockNext.mock.calls.length).toBe(1);
    expect(logger.info.mock.calls.length).toBe(1);
    expect(logger.info.mock.calls[0][0]).toEqual({
      req: mockContext.request,
      res: mockContext.response,
    });
  });

  test('throw clent error', async () => {
    const expectedError = new AppError();
    const mockContext = {
      request: 'request',
      response: 'response',
    };
    const mockNext = () => { throw expectedError; };
    await apiLogger()(mockContext, mockNext);

    expect(logger.warn.mock.calls.length).toBe(1);
    expect(logger.warn.mock.calls[0][0]).toEqual({
      req: mockContext.request,
      res: mockContext.response,
      err: expectedError,
    });
  });

  test('throw non-client error', async () => {
    const expectedError = new Error();
    const mockContext = {
      request: 'request',
      response: 'response',
    };
    const mockNext = () => { throw expectedError; };
    await apiLogger()(mockContext, mockNext);

    expect(logger.error.mock.calls.length).toBe(1);
    expect(logger.error.mock.calls[0][0]).toEqual({
      req: mockContext.request,
      res: mockContext.response,
      err: expectedError,
    });
  });
});
