module.exports = ({
  Errors,
  Joi,
  findTopicByIdService,
}) => () => async (ctx, next) => {
  const { topicId } = ctx.params;

  await Joi.validate(topicId, Joi.string().uuid());

  const topic = await findTopicByIdService(topicId);

  if (!topic) {
    throw new Errors.TopicNotFound();
  }

  await next();
};
