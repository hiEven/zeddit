module.exports = ({
  CSRF,
  Errors,
}) => () => {
  const invalidTokenMessage = 'Invalid CSRF token';

  const csrf = new CSRF({
    invalidTokenMessage,
  });

  return async (ctx, next) => {
    try {
      await csrf(ctx, next);
    } catch (err) {
      if (err.message === invalidTokenMessage) {
        throw new Errors.InvalidCSRFToken();
      }

      throw err;
    }
  };
};
