module.exports = ({
  Topic,
}) => async (id) => {
  const topic = Topic.findById(id);

  return topic;
};
