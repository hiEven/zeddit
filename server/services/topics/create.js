module.exports = ({
  Topic,
}) => async (form) => {
  const topic = await Topic.create(form);

  return topic;
};
