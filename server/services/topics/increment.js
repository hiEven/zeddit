module.exports = ({
  Topic,
}) => async (id, attrs) => {
  const topic = Topic.build({ id });

  await topic.increment(attrs);

  return topic;
};
