const findByIdFactory = require('../findById');

describe('findById', () => {
  let findById;
  let mockTopic;

  beforeEach(() => {
    mockTopic = { findById: jest.fn() };

    findById = findByIdFactory({
      Topic: mockTopic,
    });
  });

  test('success', async () => {
    const id = 'id';

    await findById(id);

    expect(mockTopic.findById.mock.calls.length).toBe(1);
    expect(mockTopic.findById.mock.calls[0]).toEqual([id]);
  });
});
