const listFactory = require('../list');

describe('list', () => {
  let list;
  let mockTopic;

  beforeEach(() => {
    mockTopic = {
      findAll: jest.fn(),
    };

    list = listFactory({
      Topic: mockTopic,
    });
  });

  test('success', async () => {
    const form = 'form';

    const expectedTopics = [1, 2, 3];
    mockTopic.findAll.mockReturnValue(expectedTopics);

    const topics = await list(form);

    expect(mockTopic.findAll.mock.calls.length).toBe(1);
    expect(mockTopic.findAll.mock.calls[0]).toEqual([form]);

    expect(topics).toEqual(expectedTopics);
  });
});
