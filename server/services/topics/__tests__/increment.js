const incrementFactory = require('../increment');

describe('increment', () => {
  let increment;
  let mockTopic;

  beforeEach(() => {
    mockTopic = {
      build: jest.fn(),
    };

    increment = incrementFactory({
      Topic: mockTopic,
    });
  });

  test('success', async () => {
    const id = 'id';
    const attrs = 'attrs';

    const mockTopicInstance = {
      increment: jest.fn(),
    };
    mockTopic.build.mockReturnValue(mockTopicInstance);

    await increment(id, attrs);

    expect(mockTopic.build.mock.calls.length).toBe(1);
    expect(mockTopic.build.mock.calls[0]).toEqual([{ id }]);

    expect(mockTopicInstance.increment.mock.calls.length).toBe(1);
    expect(mockTopicInstance.increment.mock.calls[0]).toEqual([attrs]);
  });
});
