const formatListFactory = require('../formatList');

const Promise = require('bluebird');

describe('formatList', () => {
  let formatList;
  let mockFormatTopic;

  beforeEach(() => {
    mockFormatTopic = jest.fn();

    formatList = formatListFactory({
      Promise,
      formatTopic: mockFormatTopic,
    });
  });

  test('success', async () => {
    const topics = [1, 2, 3];

    await formatList(topics);

    expect(mockFormatTopic.mock.calls.length).toBe(topics.length);
    expect(mockFormatTopic.mock.calls[0]).toEqual([topics[0]]);
  });
});
