const createFactory = require('../create');

describe('create', () => {
  let create;
  let mockTopic;

  beforeEach(() => {
    mockTopic = { create: jest.fn() };

    create = createFactory({
      Topic: mockTopic,
    });
  });

  test('success', async () => {
    const form = 'form';

    await create(form);

    expect(mockTopic.create.mock.calls.length).toBe(1);
    expect(mockTopic.create.mock.calls[0]).toEqual([form]);
  });
});
