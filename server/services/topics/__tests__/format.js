const formatFactory = require('../format');

describe('format', () => {
  let format;
  let mockTopicInstance;

  beforeEach(() => {
    mockTopicInstance = {
      toJSON: jest.fn(),
    };

    format = formatFactory();
  });

  test('success', async () => {
    await format(mockTopicInstance);

    expect(mockTopicInstance.toJSON.mock.calls.length).toBe(1);
    expect(mockTopicInstance.toJSON.mock.calls[0]).toEqual([]);
  });
});
