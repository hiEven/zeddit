module.exports = ({
  Topic,
}) => async (form) => {
  const topics = await Topic.findAll(form);

  return topics;
};
