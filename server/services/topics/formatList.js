module.exports = ({
  Promise,
  formatTopic,
}) => async topics => Promise.map(topics, topic => formatTopic(topic));
