module.exports = ({
  uuidV4,
  Joi,
  Memorm,
}) => {
  class Topic extends Memorm {
    static get schema() {
      return Joi.object({
        id: Joi.string().uuid().default(uuidV4()),
        content: Joi.string().max(255).required(),
        voteCount: Joi.number().integer().default(0),
        createdAt: Joi.date(),
        updatedAt: Joi.date(),
      });
    }

    static get prefix() {
      return 'topics';
    }

    static get orderBy() {
      return [
        'createdAt',
        'voteCount',
      ];
    }
  }

  Topic.syncStore();

  return Topic;
};
