const TopicFactory = require('../Topic');

describe('Topic', () => {
  test('success', () => {
    const Joi = require('joi');
    const uuidV4Mock = jest.fn();
    const syncStoreMock = jest.fn();
    class Memorm {
      static syncStore() {
        syncStoreMock();
      }
    }

    const Topic = TopicFactory({
      Joi,
      uuidV4Mock,
      Memorm,
    });

    expect(syncStoreMock.mock.calls.length).toBe(1);
    expect(syncStoreMock.mock.calls[0]).toEqual([]);

    // TODO: schema

    // prefix
    expect(Topic.prefix).toBe('topics');

    // orderBy
    expect(Topic.orderBy).toEqual(['createdAt', 'voteCount']);
  });
});
