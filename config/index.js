const path = require('path');
const { merge } = require('lodash');

module.exports = ({ env = 'development' }) => {
  let config = {
    name: 'zeddit',

    server: {
      port: process.env.PORT,
    },

    logger: {
      level: process.env.LOGGER_LEVEL,
    },
  };

  try {
    config = merge(config, require(path.resolve(__dirname, env))); // eslint-disable-line
  } catch (err) {
    console.error(`Load ${env} config fails`, err); // eslint-disable-line
  }

  return config;
};
