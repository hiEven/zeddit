const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: {
    bundle: [
      path.resolve(__dirname, '../src/index'),
    ],
    vendor: [
      'react',
      'react-router',
      'immutable',
      'redux',
    ],
  },

  output: {
    path: path.resolve(__dirname, '../public/build'),
    filename: '[name].js',
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: [/node_modules/],
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['es2015', 'react', 'stage-0'],
              plugins: [
                'transform-decorators-legacy',
                [
                  'transform-runtime', {
                    helpers: false,
                    polyfill: false,
                    regenerator: true,
                  },
                ],
              ],
            },
          },
        ],
      },
    ],
  },

  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
    }),
  ],

  resolve: {
    extensions: ['.js', '.jsx'],
  },
};
