import React from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import {
  Provider,
} from 'react-redux';

import App from './containers/App';
import HomePage from './containers/HomePage';

export default ({ store }) => (
  <Provider store={store}>
    <Router>
      <App>
        <Route exact path="/" component={HomePage} />
      </App>
    </Router>
  </Provider>
);
