import React from 'react';
import { Field, reduxForm } from 'redux-form';
import autobind from 'autobind-decorator';

@reduxForm({ form: 'topciCreateForm' })
export default class TopicCreateForm extends React.Component {
  render() {
    const { handleSubmit } = this.props;

    return (
      <form onSubmit={handleSubmit(this.submitForm)}>
        <div className='form-group'>
          <label htmlFor='content'>Content</label>
          <Field
            className='form-control'
            name='content'
            component='textarea'
            rows={4}
            placeholder={'What\'s on your mind?'}
          />
        </div>

        <div className='form-group'>
          <button className='btn btn-primary'>Submit</button>
        </div>
      </form>
    );
  }

  @autobind
  submitForm(data) {
    const { createTopic, reset } = this.props;

    createTopic(data);
    reset();
  }
}
