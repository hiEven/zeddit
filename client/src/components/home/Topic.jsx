import React from 'react';

export default class TopicList extends React.Component {
  render() {
    const { topic, upvoteTopic, downvoteTopic } = this.props;

    return (
      <div className='card container-fluid'>
        <div className='row'>
          <div className='col-xs-3'>
            <div className='card-block d-flex flex-column align-items-center'>
              <i
                className='fa fa-arrow-up'
                aria-hidden='true'
                onClick={upvoteTopic(topic.get('id'))}
              />
              <span>{topic.get('voteCount')}</span>
              <i
                className='fa fa-arrow-down'
                aria-hidden='true'
                onClick={downvoteTopic(topic.get('id'))}
              />
            </div>
          </div>
          <div className='col-xs-9'>
            <div className='card-block'>
              <p className='card-text'>{topic.get('content')}</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
