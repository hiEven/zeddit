import React from 'react';

import Topic from './Topic';

export default class TopicList extends React.Component {
  render() {
    const { topics, upvoteTopic, downvoteTopic } = this.props;

    return (
      <div>
        {this.renderTopicList(topics, upvoteTopic, downvoteTopic)}
      </div>
    );
  }

  renderTopicList(topics, upvoteTopic, downvoteTopic) {
    return topics.map(topic => (
      <Topic
        key={topic.get('id')}
        topic={topic}
        upvoteTopic={upvoteTopic}
        downvoteTopic={downvoteTopic}
      />
    )).toArray();
  }
}
