import { createActionTypes } from '../utils/createRequestAction';

export default createActionTypes([
  'LIST_TOPIC',
  'CREATE_TOPIC',
  'UPVOTE_TOPIC',
  'DOWNVOTE_TOPIC',
]);
