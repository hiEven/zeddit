import { Map, OrderedSet } from 'immutable';
import qs from 'qs';

export function getKey(options) {
  return qs.stringify(options) || 'default';
}

export function getPaginationByKey(state, key) {
  const pagination = state.pagination.get(key, new Map({
    loading: false,
    noMore: false,
    error: false,
    index: new OrderedSet(),
  }));

  return pagination;
}

export function getListByPagination(state, key) {
  const pagination = getPaginationByKey(state, key);

  return pagination.get('index').map(id => state.store.get(id));
}
