const BASE_TYPES = ['', 'REQUEST', 'SUCCESS', 'FAILURE'];

export const createRequestTypes = base => BASE_TYPES.reduce((acc, type) =>
  acc.concat(`${base}${type ? `_${type}` : ''}`),
  [],
);

export const createActionTypes = actionTypes => actionTypes.reduce((acc, actionType) => {
  createRequestTypes(actionType).map(requestType => (acc[requestType] = requestType));
  return acc;
}, {});
