import { call, put, select } from 'redux-saga/effects';
import { createAction } from 'redux-actions';

import { createRequestTypes } from './createRequestAction';

export default function* runAsyncAction(action, api) {
  const { type, payload, meta = {} } = action;

  if (!meta.key) meta.key = 'default';

  const [, requestActionType, successActionType, failureActionType] = createRequestTypes(type);

  const attachMeta = () => meta;
  const requestAction = createAction(requestActionType, null, attachMeta);
  const successAction = createAction(successActionType, null, attachMeta);
  const failureAction = createAction(failureActionType, null, attachMeta);

  yield put(requestAction(payload));
  const state = yield select();
  const data = yield call(api, payload, state);

  if (data instanceof Error) {
    yield put(failureAction(data, meta));
  } else {
    yield put(successAction(data, meta));
  }
}
