import { createAction } from 'redux-actions';

import ActionTypes from '../constants/actionTypes';
import { getKey } from '../selectors/common';

export const listTopic = createAction(ActionTypes.LIST_TOPIC,
  (options, refresh) => ({
    options,
    refresh,
  }),
  (options, refresh) => ({
    key: getKey(options),
    refresh,
  }),
);

export const createTopic = createAction(ActionTypes.CREATE_TOPIC,
  data => data,
  (data, options) => ({
    key: getKey(options),
  }),
);

export const upvoteTopic = createAction(ActionTypes.UPVOTE_TOPIC,
  id => ({ id }),
  id => ({ id }),
);

export const downvoteTopic = createAction(ActionTypes.DOWNVOTE_TOPIC,
  id => ({ id }),
  id => ({ id }),
);
