import { fork } from 'redux-saga/effects';

import { watchTopic } from './topics';

export default function* sagas() {
  yield [
    fork(watchTopic),
  ];
}
