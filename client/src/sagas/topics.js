import { takeEvery } from 'redux-saga/effects';

import * as topciApi from '../services/api/topics';
import ActionTypes from '../constants/actionTypes';
import runAsyncAction from '../utils/runAsyncAction';

export function* listTopic(action) {
  return yield runAsyncAction(action, topciApi.list);
}

export function* createTopic(action) {
  return yield runAsyncAction(action, topciApi.create);
}

export function* upvoteTopic(action) {
  return yield runAsyncAction(action, topciApi.upvote);
}

export function* downvoteTopic(action) {
  return yield runAsyncAction(action, topciApi.downvote);
}

export function* watchTopic() {
  yield* [
    takeEvery(ActionTypes.LIST_TOPIC, listTopic),
    takeEvery(ActionTypes.CREATE_TOPIC, createTopic),
    takeEvery(ActionTypes.UPVOTE_TOPIC, upvoteTopic),
    takeEvery(ActionTypes.DOWNVOTE_TOPIC, downvoteTopic),
  ];
}
