import { fromJS } from 'immutable';

import { getPaginationByKey } from '../selectors/common';

export function listRequestHanlder(state, action) {
  const { meta } = action;

  const pagination = getPaginationByKey(state, meta.key)
    .set('loading', true)
    .set('error', false);

  return {
    ...state,
    pagination: state.pagination.set(meta.key, pagination),
  };
}

export function listSuccessHanlder(state, action) {
  const { payload, meta } = action;

  let pagination = getPaginationByKey(state, meta.key)
    .set('loading', false)
    .set('noMore', payload.length === 0)
    .set('error', false);

  const index = pagination.get('index');
  pagination = pagination.set('index', index.concat(payload.map(item => item.id)));

  return {
    ...state,
    pagination: state.pagination.set(meta.key, pagination),
    store: state.store.withMutations((store) => {
      payload.forEach((item) => {
        store.set(item.id, fromJS(item));
      });
    }),
  };
}

export function listFailureHanlder(state, action) {
  const { meta } = action;

  const pagination = getPaginationByKey(state, meta.key)
    .set('loading', false)
    .set('error', true);

  return {
    ...state,
    pagination: state.pagination.set(meta.key, pagination),
  };
}

export function createSuccessHandler(state, action) {
  const { payload, meta } = action;

  let pagination = getPaginationByKey(state, meta.key);
  const index = pagination.get('index');

  pagination = pagination.set('index', index.add(payload.id));

  return {
    ...state,
    pagination: state.pagination.set(meta.key, pagination),
    store: state.store.set(payload.id, fromJS(payload)),
  };
}
