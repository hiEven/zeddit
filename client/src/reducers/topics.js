import { handleActions } from 'redux-actions';
import { Map } from 'immutable';

import ActionTypes from '../constants/actionTypes';
import {
  listRequestHanlder,
  listSuccessHanlder,
  listFailureHanlder,
  createSuccessHandler,
} from './common';

import {
  getPaginationByKey,
} from '../selectors/common';

export default handleActions({
  [ActionTypes.LIST_TOPIC_REQUEST]: listRequestHanlder,
  [ActionTypes.LIST_TOPIC_SUCCESS]: listSuccessHanlder,
  [ActionTypes.LIST_TOPIC_FAILURE]: listFailureHanlder,


  [ActionTypes.CREATE_TOPIC_SUCCESS]: createSuccessHandler,

  [ActionTypes.UPVOTE_TOPIC_REQUEST]: (state, action) => {
    const { meta } = action;
    const item = state.store.get(meta.id);

    const store = state.store.setIn([meta.id, 'voteCount'], item.get('voteCount') + 1);

    let pagination = getPaginationByKey(state, meta.key);
    pagination = pagination.set(
      'index',
      pagination.get('index').sort((a, b) => store.getIn([a, 'voteCount']) < store.getIn([b, 'voteCount']) ? 1 : -1),
    );

    return {
      ...state,
      pagination: state.pagination.set(meta.key, pagination),
      store,
    };
  },
  [ActionTypes.UPVOTE_TOPIC_SUCCESS]: (state, action) => {
    const { meta, payload } = action;

    let pagination = getPaginationByKey(state, meta.key);
    pagination = pagination.set(
      'index',
      pagination.get('index').sort((a, b) => state.store.getIn([a, 'voteCount']) < state.store.getIn([b, 'voteCount']) ? 1 : -1),
    );

    return {
      ...state,
      pagination: state.pagination.set(meta.key, pagination),
      store: state.store.setIn([meta.id, 'voteCount'], payload.voteCount),
    };
  },
  [ActionTypes.UPVOTE_TOPIC_FAILURE]: (state, action) => {
    const { meta } = action;
    const item = state.store.get(meta.id);

    return {
      ...state,
      store: state.store.setIn([meta.id, 'voteCount'], item.get('voteCount') - 1),
    };
  },

  [ActionTypes.DOWNVOTE_TOPIC_REQUEST]: (state, action) => {
    const { meta } = action;
    const item = state.store.get(meta.id);

    const store = state.store.setIn([meta.id, 'voteCount'], item.get('voteCount') - 1);

    let pagination = getPaginationByKey(state, meta.key);
    pagination = pagination.set(
      'index',
      pagination.get('index').sort((a, b) => store.getIn([a, 'voteCount']) < store.getIn([b, 'voteCount']) ? 1 : -1),
    );

    return {
      ...state,
      pagination: state.pagination.set(meta.key, pagination),
      store,
    };
  },
  [ActionTypes.DOWNVOTE_TOPIC_SUCCESS]: (state, action) => {
    const { meta, payload } = action;

    let pagination = getPaginationByKey(state, meta.key);
    pagination = pagination.set(
      'index',
      pagination.get('index').sort((a, b) => state.store.getIn([a, 'voteCount']) < state.store.getIn([b, 'voteCount']) ? 1 : -1),
    );

    return {
      ...state,
      pagination: state.pagination.set(meta.key, pagination),
      store: state.store.setIn([meta.id, 'voteCount'], payload.voteCount),
    };
  },
  [ActionTypes.DOWNVOTE_TOPIC_FAILURE]: (state, action) => {
    const { meta } = action;
    const item = state.store.get(meta.id);

    return {
      ...state,
      store: state.store.setIn([meta.id, 'voteCount'], item.get('voteCount') + 1),
    };
  },
}, {
  store: new Map(),
  pagination: new Map(),
});
