import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';

import app from './app';
import topics from './topics';

export default combineReducers({
  app,
  topics,
  form,
});
