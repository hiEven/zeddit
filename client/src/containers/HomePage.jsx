import React from 'react';
import { connect } from 'react-redux';
import autobind from 'autobind-decorator';

import {
  listTopic,
  createTopic,
  upvoteTopic,
  downvoteTopic,
} from '../actions/topics';

import TopicCreateForm from '../components/home/TopicCreateForm';
import TopicList from '../components/home/TopicList';

import {
  getKey,
  getListByPagination,
} from '../selectors/common';

@connect(state => (
  {
    topics: getListByPagination(state.topics, getKey()),
  }
),
  {
    listTopic,
    createTopic,
    upvoteTopic,
    downvoteTopic,
  },
)
export default class HomePage extends React.Component {
  componentWillMount() {
    const { listTopic } = this.props;
    listTopic();
  }

  render() {
    const { topics } = this.props;

    return (
      <div className='container'>
        <TopicCreateForm
          createTopic={this.createTopic}
        />
        <TopicList
          topics={topics}
          upvoteTopic={this.upvoteTopic}
          downvoteTopic={this.downvoteTopic}
        />
      </div>
    );
  }

  @autobind
  createTopic(data) {
    return this.props.createTopic(data);
  }

  @autobind
  upvoteTopic(id) {
    return () => {
      this.props.upvoteTopic(id);
    };
  }

  @autobind
  downvoteTopic(id) {
    return () => {
      this.props.downvoteTopic(id);
    };
  }
}
