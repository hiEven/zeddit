import axios from 'axios';

const HOST = '/v1/';

class ClientError extends Error {
  constructor(status, body) {
    super();
    this.status = status;
    this.body = body;
  }
}

function parseResponse(response) {
  return response.data;
}

function errorRecover(err) {
  return new ClientError(err.response.status, err.response.data);
}

export default function api(options, state) {
  const axiosOptions = {
    method: options.method,
    url: HOST + options.url,
  };

  if (options.method === 'PUT' || options.method === 'POST') {
    axiosOptions.headers = {
      'x-csrf-token': state.app.csrf,
    };
    axiosOptions.data = options.data;
  }

  return axios(axiosOptions).then(parseResponse).catch(errorRecover);
}
