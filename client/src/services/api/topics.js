import api from './api';

export const list = payload => api({
  method: 'GET',
  url: 'topics',
  params: payload,
});

export const create = (payload, state) => api({
  method: 'POST',
  url: 'topics',
  data: payload,
}, state);

export const upvote = (payload, state) => api({
  method: 'POST',
  url: `topics/${payload.id}/upvote`,
}, state);

export const downvote = (payload, state) => api({
  method: 'POST',
  url: `topics/${payload.id}/downvote`,
}, state);
