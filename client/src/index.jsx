import React from 'react';
import ReactDOM from 'react-dom';

import configureStore from './store/configureStore';
import sagas from './sagas';
import Routes from './routes';

const store = configureStore(window.$STATE);
store.runSaga(sagas);

ReactDOM.render(
  <Routes store={store} />,
  document.getElementById('root'),
);
